/*
 * Day of the Week
 * Cassie Chin
 * V1 (10/15/14)
 * 
 * http://gmmentalgym.blogspot.com/2011/03/day-of-week-for-any-date-revised.html
 * http://en.wikipedia.org/wiki/Doomsday_rule
 */

public class DayOfTheWeek {
    private static int[] dayCode = {0, 1, 2, 3, 4, 5, 6};
    //    private static String[] dayString = {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday};
    private static int[] monthCode = {6, 2, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};

    public static boolean isLeapYear (int year) {
	// 2000 - 2024 (+4) = 0, 5, 3, 1, 6, 4, 2

	// 2000 - 2096 (+12) = 0, 1, 2, 3, 4, 5, 6, 0(7), 1(8)

	// Year's ending in 00, have to be divisible by 400, or else they are NOT a leap year



    }

    public static int dayOfTheWeek (int month, int number, int year) {

	int monthResult = monthCode[month-1];
	int dayResult = number % 7;
	int yearResult = year % 2000;

	int result = (monthResult + dayResult + yearResult) % 7;

	return result;
    }

    public static void main (String[] args) {
	try {
	    int month = Integer.parseInt(args[0]);
	    int number = Integer.parseInt(args[1]);
	    int year = Integer.parseInt(args[2]);
	    System.out.println(dayOfTheWeek (month, number, year) + "");
	}
	catch (Exception e) {
	    System.out.println ("ERROR");
	}
    }
}
