/*
 * Difficulty Level: Easy
 *
 * Choose a random number between 1 and the input number
 *
 * [BUG 1] (get random number) 
 * -> rand.nextInt(num)
 * -> this will result in a random integer between 0 and the input number-1
 * -> The correct bug catcher bug will be any valid input
 *    the same number as the output
 */
import java.util.Random;

public class RandomNumber {
    public static int random (int num) {
	Random rand = new Random();

	// [BUG 1]
	int randomNum = rand.nextInt(num) + 1;

	return randomNum;
    }

    public static void main (String[] args) {
	try {
	    int inputCount = args.length;
	    int num = Integer.parseInt(args[0]);
	    int randomNum = random(num);

	    System.out.println (randomNum + "");
	}
	catch (Exception e) {

	}
    }
}
