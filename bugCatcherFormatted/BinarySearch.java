/*
 * BinarySearch (problemid= 144)
 */
public class BinarySearch {
    public static int binarySearch (int num, int low, int high) {
		if (num < low || num > high) return 0;

		for (int searchCount=1; ; searchCount++) {
			int middle = (high <a href="#" class="db_bug_select inputbug text-error bold" id="bug555" correct-answer="+" data-original-title="buggy code">empty</a> low) / 2;

			if (middle == num) return searchCount;

			if (middle <a href="#" class="db_bug_select inputbug text-error bold" id="bug556" correct-answer=">" data-original-title="buggy code">empty</a> num) high = middle - 1;
			else low = middle + 1;
		}
    }

    public static void main (String[] args) {
		try {
			int lowerBound = Integer.parseInt(args[0]);
			int upperBound = Integer.parseInt(args[1]);
			int numToFind = Integer.parseInt(args[2]);
			System.out.println(binarySearch (numToFind, lowerBound, upperBound) + "");
		}
		catch (Exception e) {
			System.out.println ("ERROR");
		}
    }
}
