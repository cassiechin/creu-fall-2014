public class CountingSort {
    // Using ROSETTA CODE'S Counting Sort Implementation: 
    // http://rosettacode.org/wiki/Sorting_algorithms/Counting_sort#Java
    public static int[] countingSort(int[] array, int min, int max){

	int[] count= new int[max - min + 1];
	for(int number : array) {
	    count[number - min]++;
	}

	int z= 0;
	for(int i= min;i <= max;i++){
	    while(count[i - min] > 0){
		array[z]= i;
		z++;
		count[i - min]--;
	    }
	}

	return array;
    }

    public static void main (String[] args) {
	try {
	    int inputCount = args.length; // The number of numbers the user inputted
	    int[] num = new int[inputCount]; // Store the user input here
	    int min = 999999; // Record the max number
	    int max = -1; // Record the min number 

	    // Read in numbers and save the max and the min number
	    for (int i=0; i<inputCount; i++) {
		num[i] = Integer.parseInt(args[i]);
		if (num[i] > max) max = num[i];
		if (num[i] < min) min = num[i];
	    }

	    // Execute Bubble Sort
	    num = countingSort (num, min, max);

	    // Print specified output
	    for (int i=0; i<inputCount; i++) System.out.print (num[i] + " ");
	    System.out.println ();
	}
	catch (Exception e) {
	    System.out.println ("ERROR");
	}
    }
}
