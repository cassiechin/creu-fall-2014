/*
 * Bubble Sort
 * Cassie Chin 
 * V1 (10/14/14)
 *
 * Difficulty: Easy (sort numbers), Hard (number of passes to sort)
 * 
 * **************************************************************************************************
 * 
 * Competition Description:
 * 
 * Bubble sort is a method of sorting an unsorted list by continuously 
 * swapping adjacent pairs until the entire list is sorted. The problem
 * is, is that even after swapping all adjacent pairs on pass through 
 * the list, the list might not be sorted. To solve this, we keep 
 * repeating passes through the list until the entire list has been sorted.
 * We know we are done when a passs through the list has no swaps because
 * it means that every adjacent pair is correctly positioned in the list.
 * 
 * This program will output the number of passes a list has to go through
 * before it is completely sorted.
 * 
 * Example
 * Input: 3 5 2 6
 * Output: 3
 * 
 * Pass 1:
 *     [3  5] 2  6  ==> 3 5 2 6
 *      3 [5  2] 6  ==> 3 2 5 6  // 5 > 2, so  swap
 *      3  2 [5  6] ==> 3 2 5 6
 * There was a swap, so we need another pass.
 *  
 * Pass 2:
 *     [3  2] 5  6  ==> 2 3 5 6  // 3 > 2, so swap
 *      2 [3  5] 6  ==> 2 3 5 6
 *      2  3 [5  6] ==> 2 3 5 6
 * There was a swap, so we need another pass.
 * 
 * Pass 3:
 *     [2  3] 5  6  ==> 2 3 5 6
 *      2 [3  5] 6  ==> 2 3 5 6
 *      2  3 [5  6] ==> 2 3 5 6
 * There were no swaps, so we are done. There were 3 passes so the output
 * should be 3
 * 
 ****************************************************************************************************
 * Test on the Command Line:
 * Setup:    Set the print flags (PRINT_SORTED_ARRAY, PRINT_NUM_PASSES) depending on what the output 
 *           should be.
 *
 * Compile:  javac Bubble.java
 * Run:      java Bubble num1 num2 num3 .. numX   # numX is any integer, you can also use any number of integers
 * 
 ****************************************************************************************************
 * Bug Catcher:
 * These are possible input/output questions.
 *     [PROBLEM A] (easy) Print out sorted set
 *     [PROBLEM B] (hard) Print out number of passes that bubble sort went through.
 *
 * Don't forget to set the print flag to true for the problem.
 *
 ****************************************************************************************************
 * Bug Fixer/Bug Catcher:
 * These are possible lines where you can put a bug.
 *     [BUG X] (Where the bug goes)            -> what to change the bug to
 *                                             -> what the bug does
 *     **********************************************************************************************
 * 
 *     [BUG 1] (Reset the swap flag)           -> swap=true; or delete this line
 *                                             -> If there is input that is already sorted, then there will be two passes instead of one
 *
 *     [BUG 2] (Go through each adjacent pair) -> for (int i=1; i<inputCount-1; i++) {
 *                                             -> The pair containing the last two numbers in the list will never be sorted
 * 
 *     [BUG 3] (If condition)                  -> if (num[i-1] < num[i]) {
 *                                             -> This will result in a backwards sorted list (highest to lowest)
 *
 *     [BUG 4] (Swap numbers)                  -> Don't use a temp, so the numbers don't get swapped
 *                                             -> The end result will be missing some numbers and some numbers will be duplicated
 * 
 *     [BUG 5] (Mark that swap happened)       -> swap=false; or delete this line
 *                                             -> There will be only one pass because the swap will not be properly acknowledged
 *
 *     [BUG 6] (While condidition)             -> (swap == false), or (swap != true)
 *                                             -> The loop will end if a swap has occured (because the swap flag would have been set to 1)
 *
 ****************************************************************************************************
 */
public class BubbleSort {
	// The number of integers that the user inputs
    public static int inputCount = 0;

    /*
     * [PROBLEM A]
     * Input: unsorted integer list separated by spaces
     * Output: sorted (lowest to highest) integer list separated by spaces
     *
     * PRINT_SORTED_ARRAY if this is true, make sure Problem B is false
     */
    private static final boolean PRINT_SORTED_ARRAY = true;

    /*
     * [PROBLEM B]
     * Input: unsorted integer list separated by spaces
     * Output: The number of passes Bubble sort needs to make
     *
     * PRINT_NUM_PASSES if this is true, make sure Problem A is false
     * passCount is the number of times the do/while loop is executed
     */
    private static final boolean PRINT_NUM_PASSES = true;
    private static int passCount = 0;

    public static int[] bubbleSort (int[]num) {
		boolean swap=true; // Flag that says whether a swap occured, true=Yes, false=No

		do {
			// Loop iteration number
			passCount = passCount+1;

			// [BUG 1]
			// reset the swap flag
			swap=false;

			// [BUG 2]
			// Go through each adjacent pair of numbers
			for (int i=1; i<inputCount; i++) {

				// [BUG 3]
				// The left number is greater than the right number
				if (num[i-1] > num[i]) {

					// [BUG 4]
					// Swap the two numbers so that the left number is less than the right number
					int temp = num[i-1];
					num[i-1] = num[i];
					num[i] = temp;

					// [BUG 5]
					// Mark that at least one swap happened during this pass
					swap=true;
				}
			}

			// [BUG 6]
			// As long as there was at least one swap during this pass, then go for another pass
		} while (swap == true);

		// There was no swaps during this pass, so return the array
		return num;
    }

    private static void printNumbers (int[] num) {
		// Print out the new numbers array
		for (int i=0; i<inputCount-1; i++) System.out.print (num[i] + " ");
		System.out.println (num[inputCount-1] + "");
    }

    public static void main (String[] args) {
		try {
			// Change string input to int and store in an array
			inputCount = args.length;
			int[] num = new int[inputCount];
			for (int i=0; i<inputCount; i++) num[i] = Integer.parseInt(args[i]);

			// Execute Bubble Sort
			num = bubbleSort (num);

			// Print specified output
			if (PRINT_SORTED_ARRAY == true) printNumbers(num);
			if (PRINT_NUM_PASSES == true) System.out.println(passCount + "");
		}
		catch (Exception e) {
			System.out.println ("ERROR");
		}
    }
}
