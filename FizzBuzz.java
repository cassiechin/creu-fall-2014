/*
 * FizzBuzz (BugFixer ONLY)
 * Cassie Chin
 * V1 (10/16/14)
 *
 * Difficulty: Easy
 * 
 ***********************************************************************
 * Competition Description:
 * 
 * The following problem is a typical interview question designed to
 * filter out 99% of prospective job candidates.
 * 
 * Write a program that prints the numbers from 1 to 100. But for 
 * multiples of three print “Fizz” instead of the number and for the 
 * multiples of five print “Buzz”. For numbers which are multiples of 
 * both three and five print “FizzBuzz”."
 * 
 ***********************************************************************
 * Test on the command line:
 * Compile : javac FizzBuzz.java
 * Run     : java FizzBuzz
 *
 ***********************************************************************
 * BugFixer:
 * These are possible bug fixer problems
 *
 * [PROBLEM A] use only if statements and NOT else if statements
 *     The program will have more than 30 output lines
 *
 * [PROBLEM B] check for (i % 15) AFTER (i % 3) and (i % 5)
 *     The program will never check for (i % 15)
 */
public class FizzBuzz {
    public static void execute () {
		for (int i=0; i<100; i++) {

			if (i % 15 == 0) System.out.println ("Fizzbuzz");	    
			else if (i % 3 == 0) System.out.println ("Fizz");
			else if (i % 5 == 0) System.out.println ("Buzz");
			else System.out.println (i + "");
		}
    }

    public static void main (String[] args) {
		execute();
    }
}
