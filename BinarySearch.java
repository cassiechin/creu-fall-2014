/*
 * BinarySearch
 * Cassie Chin
 * V1 (10/15/14)
 * 
 * Difficulty: Medium
 * 
 * *********************************************************************
 * Competition Description:
 * 
 * I'm thinking of a number between 1 and 100. You can ask me questions,
 * but the only answers I can give are "higher", "lower", or "correct".
 * The most efficient way to do this would be to keep guessing the 
 * middle number until you've got the correct answer.
 * 
 * This program takes in a number (between 1 and 100) to guess. And the
 * number of these type of questions you would have to ask to get the 
 * correct number.
 * 
 * Example 1
 *     Input: 25
 *     Output: 2
 * It would take 2 guesses to correctly find the number 25.
 * Guess 1: 50  -> "high"
 * Guess 2: 25  -> "correct"
 * 
 * Example 2
 *     Input: 37
 *     Output: 3
 * The floor function is always taken when a number is cut in half.
 * Guess 1: 50  -> "high"
 * Guess 2: 25  -> "low"
 * Guess 3: 37  -> "correct"
 * 
 ***********************************************************************
 * Test on the command line:
 * Compile : javac BinarySearch.java
 * Run     : java BinarySearch [LOWER_BOUND] [UPPER_BOUND] [SEARCH_NUM]
 *
 ***********************************************************************
 * Bug Catcher:
 * These are possible input/output questions.
 *     [PROBLEM A] (medium) Print out the number of cuts binary search 
 *                          has to make
 *
 ***********************************************************************
 * Bug Fixer/Bug Catcher:
 * These are possible lines where you can put a bug.
 *     [BUG X] (Where the bug goes)            -> what to change the bug to
 *                                             -> what the bug does
 *     **********************************************************************************************
 *
 *     [BUG 1] (compute the middle value)      -> (high - low) / 2
 *                                             -> will undercut the middle value, thus skipping some numbers
 *
 *     [BUG 2] (number is less than middle)    -> if (middle < num)
 *                                             -> **If we use [BUG 2A] do NOT use [BUG 2]
 *     
 *     [BUG 2A]                                -> switch [high = middle -1] and [low = middle + 1]
 *                                             -> **If we use [BUG 2] do NOT use [BUG 2A]
 */

public class BinarySearch {
    public static int binarySearch (int num, int low, int high) {
		if (num < low || num > high) return 0;

		for (int searchCount=1; ; searchCount++) {
			// [BUG 1]
			// Compute the middle value
			int middle = (high + low) / 2;

			// The number is found, return the search count
			if (middle == num) return searchCount;

			// [BUG 2], [BUG 2A]
			// The number is less than the middle, lower the upperbound
			if (middle > num) high = middle - 1;

			// The number is greater than the middle, raise the lowerbound
			else low = middle + 1;
		}
    }

    public static void main (String[] args) {
	try {
	    int lowerBound = Integer.parseInt(args[0]);
	    int upperBound = Integer.parseInt(args[1]);
	    int numToFind = Integer.parseInt(args[2]);
	    System.out.println(binarySearch (numToFind, lowerBound, upperBound) + "");
	}
	catch (Exception e) {
	    System.out.println ("ERROR");
	}
    }
}
