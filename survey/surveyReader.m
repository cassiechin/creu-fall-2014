1;
########################################################################
# Script to read in Bug Competition Survey's
#
# How to format the presurvey/postsurvey file (Use LibreOffice to open 
# the file and (save as a "csv" file if it is not already.)
#     1) Remove all columns except the columns specified in "categoryName"
#     2) Find/Replace all 0 with 6
#     3) Find/Replace all empty cells with -1
#     4) Ensure that the file is a csv separated by commas NOT tabs
#        (Open in a text editor to check this)
#     5) For the postsurvey file, you must add a column of zeros for the
#        corresponding "Write Computer Programs" file that is in the
#        presurvey
#     6) Remove the category labels at the top
#
# How to run the surveyReader?
#     octave surveyReader.m [presurvey.csv] [postsurvey.csv] 
#
# TODO:
# - Automatic formatting of input file
# - Add ability to analyze multiple types of input (right now, we can 
#   only analyze a category if it has a possible result of 0-5), we can't
#   do yes/no/maybe questions
# - Automatic plot generation
# - Nicer output formatting
# - More statistical analysis
# - Output to a file so we don't have to pipe out
########################################################################

display ("============================================================")
display ("                Bug Competition Survey Analysis             ")
display ("============================================================")

# Octave does not support 0 indices, so put zero in this index
# this number has to be something that is not already used as a result
# Right now we have 1-5 as possible results, so we have to set 0 to go
# into index 6
putZeroInThisIndex = 6;

names=argv();

# Read in the presurvey
presurvey=names{1}
data_presurvey=csvread(presurvey);
size_presurvey=size(data_presurvey,1)

# Read in the post survey
postsurvey=names{2}
data_postsurvey=csvread(postsurvey);
size_postsurvey=size(data_postsurvey,1)

categoryName = {"Software Testing",
		"Problem Solving",
		"Coding",
		"Computer Science in General",
		"Write Computer Programs", # Only in the pre-survey
		"Perform Software Testing",
		"Pursue Career in Software Testing"};
				
# presurvey and post survey should have the same number of columns
for j=1:columns(data_presurvey)
	# put the results here
	results_presurvey=[0,0,0,0,0,0];
	results_postsurvey=[0,0,0,0,0,0];

	# go through each row in the column
	for i=1:size(data_presurvey,1) 
		number_presurvey = data_presurvey(i,j); # the value of the cell
		
		# you can't have a 0 index in octave, so put all this in index 6
		if (number_presurvey == 0)
			number_presurvey = putZeroInThisIndex;
		endif

		# increment the counter
		if (number_presurvey != -1)
		 results_presurvey(number_presurvey) = results_presurvey(number_presurvey) + 1;
		endif
	endfor
	
	for i=1:size(data_postsurvey,1) 
		number_postsurvey = data_postsurvey(i,j);
		if (number_postsurvey == 0)
			number_postsurvey = putZeroInThisIndex;
		endif	
		if (number_postsurvey != -1) 
			results_postsurvey(number_postsurvey) = results_postsurvey(number_postsurvey) + 1;		
		endif	
	endfor
	
	# show the results for this column
	totalvotes_presurvey=0;
	totalvotes_postsurvey=0;
	for i=1:columns(results_presurvey)
		totalvotes_presurvey=totalvotes_presurvey+results_presurvey(i);
		totalvotes_postsurvey=totalvotes_postsurvey+results_postsurvey(i);
	endfor
	
	display ("============================================================")
	categoryName{j} # display the category
	
	display ("\nPRESURVEY RESULTS")
	votes=[];
	percentages=[];
	for a=1:columns(results_presurvey)
		votes=horzcat(votes, results_presurvey(a));
		percentage=results_presurvey(a)/totalvotes_presurvey * 100;
		percentages=horzcat(percentages, percentage);
	endfor	
	disp("The number of votes for options (1,2,3,4,5,0) are"), disp(votes)
	disp("The percentage of votes for options (1,2,3,4,5,0) are"), disp(percentages)
	
	display ("\nPOSTSURVEY RESULTS")
	votes=[];
	percentages=[];
	for a=1:columns(results_presurvey)
		votes=horzcat(votes, results_postsurvey(a));
		percentage=results_postsurvey(a)/totalvotes_postsurvey * 100;
		percentages=horzcat(percentages, percentage);
	endfor
	disp("The number of votes for options (1,2,3,4,5,0) are"), disp(votes)
	disp("The percentage of votes for options (1,2,3,4,5,0) are"), disp(percentages)
endfor
