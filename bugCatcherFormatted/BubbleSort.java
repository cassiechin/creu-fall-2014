/*
 * Bubble Sort (problemid= 145)
 */
public class BubbleSort {
	// The number of integers that the user inputs
    public static int inputCount = 0;

	// The number of passes needed to complete Bubble Sort
    private static int passCount = 0;

    public static int[] bubbleSort (int[]num) {
		// Flag that says whether a swap occured, true=Yes, false=No
		boolean swap=<a href="#" class="db_bug_select inputbug text-error bold" id="bug558" correct-answer="true" data-original-title="buggy code">empty</a>; 
		
		do {
			// Loop iteration number
			passCount = passCount+1;

			// reset the swap flag
			swap=<a href="#" class="db_bug_select inputbug text-error bold" id="bug" correct-answer="false" data-original-title="buggy code">empty</a>;

			// Go through each adjacent pair of numbers
			for (int i=1; i<inputCount; i++) {

				// The left number is greater than the right number
				if (num[i-1] <a href="#" class="db_bug_select inputbug text-error bold" id="bug" correct-answer=">" data-original-title="buggy code">empty</a> num[i]) {

					// Swap the two numbers so that the left number is less than the right number
					int temp = num[i-1];
					num[i-1] = num[i];
					num[i] = temp;

					// Mark that at least one swap happened during this pass
					swap=<a href="#" class="db_bug_select inputbug text-error bold" id="bug558" correct-answer="true" data-original-title="buggy code">empty</a>;
				}
			}

			// As long as there was at least one swap during this pass, then go for another pass
		} while (swap == <a href="#" class="db_bug_select inputbug text-error bold" id="bug558" correct-answer="true" data-original-title="buggy code">empty</a>);

		// There was no swaps during this pass, so return the array
		return num;
    }

    public static void main (String[] args) {
		try {
			// The number of input numbers
			inputCount = args.length;
			
			// Change string input to int and store in an array
			int[] num = new int[inputCount];
			for (int i=0; i<inputCount; i++) num[i] = Integer.parseInt(args[i]);

			// Execute Bubble Sort
			num = bubbleSort (num);

			// Print specified output
			System.out.println(passCount + "");
		}
		catch (Exception e) {
			System.out.println ("ERROR");
		}
    }
}
